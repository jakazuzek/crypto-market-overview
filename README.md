# Crytocurrency Market Overview

Crytocurrency Market Overview is an app that fetches data from Bitstamp's API for selected crypto currency pairs and selected timespan and then displays the data in an area chart. Additionaly it calculates min and max trading value of the crypto currency for selected timespan, plus the current price.

## Start the app localy

Install node_modules and start the app.

```
yarn install
yarn start
```