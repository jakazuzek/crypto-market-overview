import React from 'react';
import MarketOverview from './pages/MarketOverview';
import './App.scss';

function App(props: any) {
  return (
    <div className="App">
      <MarketOverview />
    </div>
  );
}

export default App;