import React from 'react';
import { Main, TimeSpan } from '../../utils/types';
import { timeSpans } from '../../utils/static';
import { useDispatch, useSelector } from 'react-redux';
import { setTimespan } from '../../store/actions';
import './style.scss';

function TimeSpanSelector() {
    const timeSpan = useSelector<Main, TimeSpan>(state => state.timespan);
    const dispatch = useDispatch();

    return (
        <div className="timespans-selector">
            {Object.keys(timeSpans).map(item => {
                return (
                    <button
                        key={timeSpans[item].shortName}
                        className={timeSpans[item].shortName === timeSpan.shortName? 'active': ''}
                        onClick={() => dispatch(setTimespan(item))}
                    >
                        {timeSpans[item].shortName}
                    </button>
                );
            })}
        </div>
    );
}

export default TimeSpanSelector;