import React from "react";
import { Area, AreaChart, ResponsiveContainer, XAxis, YAxis } from "recharts";

interface Props {
    data: {x: number, y:number}[];
}

function AreaChartMini({
    data
}: Props) {
    return (
        <ResponsiveContainer>
            <AreaChart
                data={data}
                margin={{
                    top: 10,
                    right: 10,
                    left: 10,
                    bottom: 10,
                }}
            >
                <defs>
                    <linearGradient id="color" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
                    <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
                    </linearGradient>
                </defs>
                <XAxis dataKey="x" hide />
                <YAxis type="number" domain={['dataMin', 'dataMax']} hide />
                {/* <Tooltip /> */}
                <Area type="monotone" dataKey="y" stroke="#8884d8" fillOpacity={1} fill="url(#color)" />
            </AreaChart>
        </ResponsiveContainer>
    );
}

export default AreaChartMini;