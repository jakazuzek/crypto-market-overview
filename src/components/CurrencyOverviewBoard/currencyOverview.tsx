import React from 'react';
import { useSelector } from 'react-redux';
import { formatNumber } from '../../utils';
import { useFetch, usePriceDiffCalculation } from '../../utils/customHooks';
import { Main, OHLCResponse, TimeSpan } from '../../utils/types';
import AreaChartMini from './AreaChart';
import './style.scss';

interface Props {
    timeSpan: TimeSpan;
    currencyPair: string;
    currency: string;
    id: number;
}

function CurrencyOverview({
    timeSpan,
    currencyPair,
    currency,
    id,
}: Props): JSX.Element {
    const loading = useSelector<Main, boolean>(state => state.loading);
    const data = useSelector<Main, OHLCResponse|undefined>(state => state.ohlcData[id]);

    useFetch(timeSpan, currencyPair, id);
    const priceDiff = usePriceDiffCalculation(data||undefined);
    return (
        <div className="currency-overview-container">
            {loading?
                <div className="lds-ellipsis"><div/><div/><div/><div/></div>
            :    
                <>
                    <div className="currency-pair">{data && data.pair && data.pair.replace(`/${currency}`, '')}</div>
                    <div className="currency-chart">
                        {(data && data.ohlc && data.ohlc.length > 0) &&
                            <AreaChartMini
                                data={
                                    data.ohlc.map(item => (
                                        {x: +item.timestamp, y: +item.close}
                                    ))
                                }
                            />
                        }
                    </div>
                    <div className="currency-price-info">
                        <div>{currency} {(data && data.ohlc && data.ohlc.length > 0) &&
                            formatNumber(+data.ohlc[data.ohlc.length - 1].close).value}</div>
                        <div
                            className={priceDiff.priceDiffAmount?.connotation === '+'? 'positive': 'negative'}
                        >
                            {(priceDiff.priceDiffAmount?.connotation)} {currency} {(priceDiff.priceDiffAmount?.value)}
                        </div>
                        <div
                            className={priceDiff.priceDiffPercentage?.connotation === '+'? 'positive': 'negative'}
                        >
                            {priceDiff.priceDiffPercentage?.connotation} {priceDiff.priceDiffPercentage?.value} %
                        </div>
                    </div>
                </>
            }
        </div>
    );
}

export default CurrencyOverview;