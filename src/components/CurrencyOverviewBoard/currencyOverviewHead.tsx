import React from "react";

interface Props {
    timeSpanName: string
}

function CurrencyOverviewHead({timeSpanName}: Props) {
    return (
        <div className="currency-overview-container header">
            <span>Cryptocurrency</span>
            <span>
                <span>Current Price</span>
                <span> | </span>
                <span>Last {timeSpanName}</span>
            </span>
        </div>
    );
}

export default CurrencyOverviewHead;