import React from 'react';
import { useSelector } from 'react-redux';
import { useCurrencyPairs } from '../../utils/customHooks';
import { initialCurrencies } from '../../utils/static';
import { Main, TimeSpan } from '../../utils/types';
import CurrencyOverview from './currencyOverview';
import CurrencyOverviewHead from './currencyOverviewHead';
import './style.scss';

function CurrencyOverviewBoard(): JSX.Element {
    const timeSpan = useSelector<Main, TimeSpan>(state => state.timespan);
    const currency = useSelector<Main, string>(state => state.currency);
    const currencyPairs = useCurrencyPairs(currency, initialCurrencies);

    return (
        <div className="currency-overview-wrappers">
            <CurrencyOverviewHead timeSpanName={timeSpan.longName}/>
            {currencyPairs && currencyPairs.map((pair, index) => 
                <CurrencyOverview key={pair} id={index} timeSpan={timeSpan} currencyPair={pair} currency={currency}/>
            )}
        </div>
    );
}

export default CurrencyOverviewBoard;