import { OHLCResponse } from "../../utils/types";

export const setCurrency = (currency: string) => {
    return {
        type: 'CHANGE_CURRENCY',
        payload: currency,
    }
};

export const setTimespan = (type: string) =>  {
    return { type };
};

export const loading = (type: string) =>  {
    return { type };
};

export const storeOhlcData = (payload: OHLCResponse, id: number) =>  {
    return { 
        type: 'SAVE_OHLC_DATA',
        payload,
        id,
    };
};