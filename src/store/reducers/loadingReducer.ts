export const loadingReducer = (state: boolean = false, action: {type: string}) => {
    switch(action.type) {
        case 'START_LOADING': {
            return true;
        }
        case 'STOP_LOADING': {
            return false;
        }
        default: {
            return state;
        }
    }
};