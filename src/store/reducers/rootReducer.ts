import { combineReducers } from 'redux';
import { currencyReducer } from './currencyReducer';
import { loadingReducer } from './loadingReducer';
import { ohlcDataReducer } from './ohlcDataReducer';
import { timespanReducer } from './timespanReducer';

export default combineReducers({
    currency: currencyReducer,
    timespan: timespanReducer,
    loading: loadingReducer,
    ohlcData: ohlcDataReducer,
});