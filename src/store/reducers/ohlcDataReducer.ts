import {  Main, OHLCResponse } from "../../utils/types";

export const ohlcDataReducer = (state: Main['ohlcData'] = {}, action: {type: string, payload: OHLCResponse, id: number}) => {
    if (action.payload) {
        switch(action.type) {
            case 'SAVE_OHLC_DATA': {
                const newState = {...state};
                newState[action.id] = action.payload
                return newState;
            }
            default: {
                return state;
            }
        }
    }
    return state;
};