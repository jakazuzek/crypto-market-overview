import { timeSpans } from "../../utils/static";
import {  TimeSpan } from "../../utils/types";

export const timespanReducer = (state: TimeSpan = timeSpans['3_HOURS'], action: {type: string}) => {
    if (timeSpans[action.type]) return timeSpans[action.type];
    return state;
};