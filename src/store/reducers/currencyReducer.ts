export const currencyReducer = (state = 'EUR', action: any) => {
    switch (action.type) {
        case 'CHANGE_CURRENCY':
            return action.payload;
        default:
            return state;
    }
};