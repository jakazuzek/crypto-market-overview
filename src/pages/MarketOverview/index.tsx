import React from 'react';
import CurrencyOverviewBoard from '../../components/CurrencyOverviewBoard';
import TimeSpanSelector from '../../components/TimeSpanSelector';

function MarketOverview() {
    return (
        <div className="">
            <header>
                <h1>Market Overview</h1>
            </header>
            
            <TimeSpanSelector />
            <CurrencyOverviewBoard />
        </div>
    )
}

export default MarketOverview;
