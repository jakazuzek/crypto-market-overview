/**
 * calculatePriceDiff function that calculates difference between 2 values
 * @param {number} value1 initial value
 * @param {number} value2 new value
 * @returns returns object with amount and percentage values
 */
export function calculatePriceDiff(value1: number, value2: number) {
    const priceDiffAmount: number = (+value2) - (+value1) // Calculate price difference between opening and closing price
    const priceDiffPercentage = (priceDiffAmount / (+value1)) * 100 // Calculate price difference percantage

    return {
        priceDiffAmount: formatNumber(priceDiffAmount),
        priceDiffPercentage: formatNumber(priceDiffPercentage)
    }
}

/**
 * formatNumber formats number to desired decimals number and also returns the connotation (+/-) separately
 * @param num number to format
 * @param decimals [optional] number of decimals, default=2
 * @returns object of formated number and connotation
 */
export function formatNumber(num: number, decimals: number = 2): {connotation: string, value: string} {
    let formatedNumber;
    if (decimals === 0) formatedNumber = Math.round(num);
    else formatedNumber = (Math.round(num * (10 ** decimals)) / (10 ** decimals));

    return {
        connotation: num >= 0 ? '+' : '-',
        value: Math.abs(formatedNumber).toFixed(decimals)
    };
}