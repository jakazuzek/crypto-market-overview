import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { calculatePriceDiff } from '.';
import { loading, storeOhlcData } from '../store/actions';
import { OHLCResponse, TimeSpan } from './types';

/**
 * useCurrencyPairs hook creates new currency pairs and returns them, hook fires on every currency change
 * @param currency
 * @param currencies
 * @returns array of currency pairs
 */
export function useCurrencyPairs(currency: string, currencies: string[]) {
    const [currencyPairs, setCurrencyPairs] = useState<string[]>([]);
    useEffect(() => {
        if (currency && currencies) {
            setCurrencyPairs(currencies.map(item => `${item}${currency.toLowerCase()}`))
        }
    }, [currency])
    return currencyPairs;
}

/**
 * useFetch hook fetches OHLC data and returns the response, additionaly it tracks the progress of fetching with loading parameter. Hook will fire on every timespan and/or currencyPair change
 * @param step TimeSpan
 * @param currencyPair 
 * @returns 
 */
export function useFetch(step: TimeSpan, currencyPair: string, id: number) {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loading('START_LOADING'));
        fetch(`https://www.bitstamp.net/api/v2/ohlc/${currencyPair}/?step=${step.seconds}&limit=${step.limit}`)
            .then(res => res.json())
            .then(res => {
                dispatch(storeOhlcData(res.data, id))
                dispatch(loading('STOP_LOADING'));
            })
    }, [JSON.stringify(step)]);
}

/**
 * usePriceDiffCalculation hook returns value of calculatePriceDiff when new data is fetched
 * @param data ohlc data
 * @returns 
 */
export function usePriceDiffCalculation(data: OHLCResponse | undefined) {
    const [state, setState] = useState<{ priceDiffAmount?: {connotation: string, value: string}, priceDiffPercentage?: {connotation: string, value: string} }>({});

    useEffect(() => {
        if (data && data.ohlc && data.ohlc) {
            const firstOpeningPrice = +data.ohlc[0].open;
            const lastClosingPrice = +data.ohlc[data.ohlc.length - 1].close
            setState(calculatePriceDiff(firstOpeningPrice, lastClosingPrice))
        }
    }, [JSON.stringify(data)])

    return state;
}