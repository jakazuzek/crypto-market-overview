export interface TimeSpan {
    shortName: string;
    longName: string;
    seconds: 60|180|300|900|1800|3600|7200|14400|21600|43200|86400|259200;
    limit: number;
}

export interface OHLCResponse {
    pair: string;
    ohlc: {
        high: string;
        timestamp: string;
        volume: string;
        low: string;
        close: string;
        open: string;
    }[];
}

export interface Main {
    currency: string;
    timespan: TimeSpan;
    ohlcData: {[key:number]: OHLCResponse};
    loading: boolean;
}