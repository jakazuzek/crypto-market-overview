// import React from 'react';
// import { render, screen } from '@testing-library/react';
// import App from './App';

// test('renders learn react link', () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

import React from 'react';
import TestRenderer from 'react-test-renderer';
import CurrencyOverviewHead from './components/CurrencyOverviewBoard/currencyOverviewHead';
import { formatNumber } from './utils';


describe('App', () => {
  it('renders correctly', () => {
    const tree = TestRenderer.create(
      <CurrencyOverviewHead timeSpanName="24H" />,
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

test('formatNumber', () => {
  expect(formatNumber(0, 0)).toEqual({
    connotation: '+',
    value: '0'
  });
  expect(formatNumber(0)).toEqual({
    connotation: '+',
    value: '0.00'
  });
  expect(formatNumber(1000)).toEqual({
    connotation: '+',
    value: '1000.00'
  });
  expect(formatNumber(1234.1234)).toEqual({
    connotation: '+',
    value: '1234.12'
  })
  expect(formatNumber(-10.29876, 3)).toEqual({
    connotation: '-',
    value: '10.299'
  })
});
